// після завершення завантаження сторінки
$(document).ready(function(){
    $('.pic a').lightBox({
        // викликаємо lightBox і конвертуємо усі посилання в контейнері .pic в галерею Lightbox
        imageLoading: 'lightbox/images/loading.gif',
        imageBtnClose: 'lightbox/images/close.gif',
        imageBtnPrev: 'lightbox/images/prev.gif',
        imageBtnNext: 'lightbox/images/next.gif'
    });
});
